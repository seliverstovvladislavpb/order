DROP TABLE IF EXISTS t_order_items CASCADE;
DROP TABLE IF EXISTS t_orders CASCADE;
DROP TABLE IF EXISTS t_products CASCADE;
DROP TABLE IF EXISTS t_categories CASCADE;

CREATE TABLE "t_categories" (
                                "pk_id" serial NOT NULL,
                                "c_name" TEXT NOT NULL UNIQUE,
                                CONSTRAINT "t_categories_pk" PRIMARY KEY ("pk_id")
) WITH (
      OIDS=FALSE
    );



CREATE TABLE "t_products" (
                              "pk_id" serial NOT NULL,
                              "c_name" TEXT NOT NULL UNIQUE,
                              "c_sku" TEXT NOT NULL UNIQUE,
                              "c_price" FLOAT NOT NULL,
                              "fk_category_id" integer NOT NULL,
                              CONSTRAINT "t_products_pk" PRIMARY KEY ("pk_id")
) WITH (
      OIDS=FALSE
    );



CREATE TABLE "t_orders" (
                            "pk_id" serial NOT NULL,
                            "c_date" DATE NOT NULL,
                            "c_amount" FLOAT NOT NULL,
                            CONSTRAINT "t_orders_pk" PRIMARY KEY ("pk_id")
) WITH (
      OIDS=FALSE
    );



CREATE TABLE "t_order_items" (
                                 "pk_id" serial NOT NULL,
                                 "fk_order_id" integer NOT NULL,
                                 "fk_product_id" integer NOT NULL,
                                 "c_quantity" integer NOT NULL,
                                 CONSTRAINT "t_order_items_pk" PRIMARY KEY ("pk_id")
) WITH (
      OIDS=FALSE
    );




ALTER TABLE "t_products" ADD CONSTRAINT "t_products_fk0" FOREIGN KEY ("fk_category_id") REFERENCES "t_categories"("pk_id");


ALTER TABLE "t_order_items" ADD CONSTRAINT "t_order_items_fk0" FOREIGN KEY ("fk_order_id") REFERENCES "t_orders"("pk_id");
ALTER TABLE "t_order_items" ADD CONSTRAINT "t_order_items_fk1" FOREIGN KEY ("fk_product_id") REFERENCES "t_products"("pk_id");
