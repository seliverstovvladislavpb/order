<!DOCTYPE html>
<html>

<head>
    <title>Order</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<header>
    <div>
        <table>
            <tr>
                <th>
                    <form action="/shop/all-products" method="Get">
                        <input type="submit" value="Shop">
                    </form>
                </th>
            </tr>
        </table>
    </div>
</header>
<h2>Order</h2>
<table class="table table-bordered">
    <tr>
        <th>Product</th>
        <th>Category</th>
        <th>SKU</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Delete</th>
    </tr>

    <#if items??>
        <#list items as item>
            <tr>
                <td>
                    ${item.getProduct().getName()}
                </td>
                <td>
                    ${item.getProduct().getCategory().getName()}
                </td>
                <td>
                    ${item.getProduct().getSku()}
                </td>
                <td>
                    ${item.getQuantity()}
                </td>
                <td>
                    ${item.getProduct().getPrice()*item.getQuantity()}
                </td>
                <td>
                    <form action="/shop/delete/${item.getProduct().getSku()}" method="get">
                        <input type="submit" value="Delete">
                    </form>
                </td>
            </tr>
        </#list>
    </#if>
    <br>
</table>
<h3>
    Total amount: ${orderAmount}
</h3>
<br><br>
<form action="/order/execute" method="post">
    <input type="submit" value="Execute order">
</form>
<br><br>
<form action="/order/discard" method="get">
    <input type="submit" value="Discard order">
</form>
</body>
</html>