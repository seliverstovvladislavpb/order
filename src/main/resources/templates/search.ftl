<!DOCTYPE html>
<meta charset="UTF-8">
<title>Products</title>
</head>
<body>
<h2>Search product</h2>
<form action="/search/order/result" method="get">
    Product name:<br>
    <input type="text" name="productName">
    <input type="submit" value="Search">
</form>
<#if results??>
    <#list results as result>
        <br>
        ${result.toString()}
    </#list>
</#if>
</body>
</html>
