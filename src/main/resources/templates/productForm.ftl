<!DOCTYPE html>
<meta charset="UTF-8">
<title>Products</title>
</head>
<body>
<h2>Add new product</h2>
<form action="/products/add" method="post">
    Product name:<br>
    <input type="text" name="name">
    <br><br>
    Product sku:<br>
    <input type="text" name="sku">
    <br><br>
    Product price:<br>
    <input type="number" step="0.1" name="price">
    <br><br>
    Category:<br>
    <select path="category" name="category">
        <#list categories as category>
            <option value = "${category.getId()}" label = "${category.getName()}"/>
        </#list>
    </select>
    <br><br>
    <input type="submit" value="Add">
</form>
</body>
</html>
