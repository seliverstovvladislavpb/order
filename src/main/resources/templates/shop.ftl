<!DOCTYPE html>
<html>

<head>
    <title>Shop</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<header>
    <div>
        <table>
            <tr>
                <th>
                    <form action="/order/show" method="get">
                        <input type="submit" value="Order">
                    </form>
                </th>
            </tr>
        </table>
    </div>
</header>
<h2>Shop</h2>
<table class="table table-bordered">
    <tr>
        <th>Product</th>
        <th>Category</th>
        <th>SKU</th>
        <th>Price</th>
        <th>Buy</th>
    </tr>

    <#list products as product>
        <tr>
            <td>
                ${product.getName()}
            </td>
            <td>
                ${product.getCategory().getName()}
            </td>
            <td>
                ${product.getSku()}
            </td>
            <td>
                ${product.getPrice()}
            </td>
            <td>
                <form action="/shop/buy/${product.getId()}" method="get">
                    <input type="number" name="quantity">
                    <input type="submit" value="Buy">
                </form>
            </td>
        </tr>
    </#list>
</table>
</body>
</html>