insert into t_categories (c_name) values ('електроника');
insert into t_categories (c_name) values ('книги');
insert into t_categories (c_name) values ('спорт');
insert into t_categories (c_name) values ('досуг');

insert into t_products (c_name, c_sku, c_price, fk_category_id) values ('product1', 'sku1', 123, 1);
insert into t_products (c_name, c_sku, c_price, fk_category_id) values ('product2', 'sku2', 500, 2)