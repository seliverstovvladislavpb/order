package com.order.project.specifications;

import com.order.project.model.Order;
import com.order.project.model.OrderItem;
import com.order.project.model.Product;
import org.hibernate.Criteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class OrderItemSpecification implements Specification<OrderItem> {

    private SearchCriteria criteria;

    public OrderItemSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<OrderItem> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Join<Product, OrderItem> productJoin = root.join("product");
        Predicate itemByProductNamePredicate = criteriaBuilder.equal(productJoin.get("name"), criteria.getValue());
        return itemByProductNamePredicate;
    }
}
