package com.order.project.controller;

import com.order.project.model.Product;
import com.order.project.service.impl.SearchService;
import com.order.project.specifications.ProductSpecification;
import com.order.project.specifications.SearchCriteria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {

    private SearchService service;

    public SearchController(SearchService service) {
        this.service = service;
    }

    @GetMapping("/order")
    public String showSearchItemWindow(Model model) {
        return "search";
    }

    @GetMapping("/order/result")
    public String findOrderItemByProduct(String productName, Model model) {
        model.addAttribute("results", service.findOrderByProductName(productName));
        return "search";
    }
}
