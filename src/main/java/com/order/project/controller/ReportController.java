package com.order.project.controller;

import com.order.project.model.Order;
import com.order.project.service.IOrderService;
import com.order.project.service.impl.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/report")
public class ReportController {

    IOrderService orderService;

    @Autowired
    public ReportController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/show")
    public @ResponseBody
    Map<LocalDate, Float> showReport(Model model) {
        Map<LocalDate, Float> reportMap = orderService.getReportData();

        return reportMap;
    }
}
