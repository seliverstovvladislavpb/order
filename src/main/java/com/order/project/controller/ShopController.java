package com.order.project.controller;

import com.order.project.model.OrderItem;
import com.order.project.service.IOrderItemService;
import com.order.project.service.IProductService;
import com.order.project.service.impl.OrderItemService;
import com.order.project.service.impl.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/*
Controller for shop window with all products
 */
@Controller
@RequestMapping("/shop")
public class ShopController {

    private IProductService productService;
    private IOrderItemService orderItemService;

    @Autowired
    public ShopController(ProductService productService,
                          OrderItemService orderItemService) {
        this.productService = productService;
        this.orderItemService = orderItemService;
    }

    @GetMapping("/all-products")
    public String showAllProducts(Model model)
    {
        model.addAttribute("products", productService.findAll());
        return "shop";
    }

    @GetMapping("/buy/{productId}")
    public String addOrderItem(OrderItem item, @PathVariable("productId")Long productId, Model model) {
        item.setProduct(productService.findById(productId));
        orderItemService.addOrderItemToLocalStorage(item);
        model.addAttribute("products", productService.findAll());
        return "shop";
    }

    @GetMapping("/delete/{productSku}")
    public String deleteOrderItem(@PathVariable("productSku")String productSku, Model model) {
        orderItemService.deleteOrderItemFromLocalStorageByProductSku(productSku);
        model.addAttribute("products", productService.findAll());
        return "redirect:/order/show";
    }
}
