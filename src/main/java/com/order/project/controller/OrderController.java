package com.order.project.controller;

import com.order.project.model.Order;
import com.order.project.service.IOrderItemService;
import com.order.project.service.IOrderService;
import com.order.project.service.impl.OrderItemService;
import com.order.project.service.impl.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/*
Controller for window with current order.
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    private IOrderService orderService;
    private IOrderItemService orderItemService;

    @Autowired
    public OrderController(OrderService orderService, OrderItemService orderItemService) {
        this.orderService = orderService;
        this.orderItemService = orderItemService;
    }

    @GetMapping("/info/{id}")
    public @ResponseBody
    String info(@PathVariable("id") Long id){
        return orderService.findById(id).toString();
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        orderService.deleteById(id);
    }

    @GetMapping("/show")
    public String showOrder(Model model) {
        model.addAttribute("items", orderItemService.getOrderItemsFromLocalStorage());
        model.addAttribute("orderAmount", orderItemService.calculateTotalAmount());
        return "order";
    }

    @PostMapping("/execute")
    public String executeOrder() {
        Order order = orderService.createOrder(orderItemService.calculateTotalAmount());
        orderItemService.applyOrderToItemsInLocalStorage(order);
        orderItemService.saveAll();
        return "redirect:/shop/all-products";
    }

    @GetMapping("/discard")
    public String discardOrder() {
        orderItemService.setOrderItemsToLocalStorage(null);
        return "redirect:/shop/all-products";
    }
}
