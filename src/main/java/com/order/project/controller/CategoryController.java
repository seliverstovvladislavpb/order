package com.order.project.controller;

import com.order.project.model.Category;
import com.order.project.service.ICategoryService;
import com.order.project.service.impl.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/category")
public class CategoryController {

    ICategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/info/{id}")
    public @ResponseBody
    String info(@PathVariable("id") Long id) {
        return categoryService.findById(id).toString();
    }

    @PostMapping("/add")
    public String save(Category category) {
        return categoryService.save(category).toString();
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") Long id) {
        categoryService.deleteById(id);
    }

    @GetMapping("/update/{id}")
    public String update(Category category, @PathVariable("id") Long id) {
        return categoryService.update(category, id).toString();
    }
}
