package com.order.project.controller;

import com.order.project.model.Category;
import com.order.project.model.Product;
import com.order.project.service.impl.CategoryService;
import com.order.project.service.impl.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductController {

    private ProductService productService;
    private CategoryService categoryService;

    private List<Category> categoryList = null;

    @Autowired
    public ProductController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @GetMapping("/add")
    public String showForm(Model model) {
        if(categoryList == null){
            categoryList = categoryService.findAll();
        }
        model.addAttribute("categories", categoryList);
        return "productForm";
    }

    @PostMapping("/add")
    public String save(Product product, Model model)
    {
        productService.save(product);
        model.addAttribute("categories", categoryList);
        return "productForm";
    }

    @PostMapping("/update/{id}")
    public String update(Product product, @PathVariable("id") Long id) {
        productService.update(product, id);
        return "redirect:/shop";
    }

    @GetMapping("/info")
    public @ResponseBody Product info(Long id) {
        return productService.findById(id);
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id){
        productService.deleteById(id);
        return "redirect:/shop";
    }
}
