package com.order.project.service;

import com.order.project.model.Order;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface IOrderService {
    List<Order> findAll();
    Order findById(Long id);
    void deleteById(Long id);
    Order createOrder(Float totalAmount);
    Order update(Order order, Long id);
    Map<LocalDate, Float> getReportData();
}
