package com.order.project.service;

import com.order.project.model.Order;
import com.order.project.model.OrderItem;

import java.util.List;

public interface IOrderItemService {

    List<OrderItem> findAll();
    OrderItem findById(Long id);
    void deleteById(Long id);
    OrderItem save(OrderItem item);
    void saveAll();
    OrderItem update(OrderItem item, Long id);
    void addOrderItemToLocalStorage(OrderItem newItem);
    void deleteOrderItemFromLocalStorageByProductSku(String productSku);
    void applyOrderToItemsInLocalStorage(Order order);
    Float calculateTotalAmount();
    List<OrderItem> getOrderItemsFromLocalStorage();
    void setOrderItemsToLocalStorage(List<OrderItem> orderItems);

}
