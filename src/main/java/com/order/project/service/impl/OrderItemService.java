package com.order.project.service.impl;

import com.order.project.model.Order;
import com.order.project.model.OrderItem;
import com.order.project.repository.OrderItemRepository;
import com.order.project.service.IOrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderItemService implements IOrderItemService {

    private OrderItemRepository repository;

    List<OrderItem> orderItems;

    public List<OrderItem> getOrderItemsFromLocalStorage() {
        return orderItems;
    }

    public void setOrderItemsToLocalStorage(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Autowired
    public OrderItemService(OrderItemRepository repository) {
        orderItems = null;
        this.repository = repository;
    }

    @Override
    public List<OrderItem> findAll() {
        return (List<OrderItem>) repository.findAll();
    }

    @Override
    public OrderItem findById(Long id) {
        Optional<OrderItem> orderItemOptional = repository.findById(id);
        if(orderItemOptional.isPresent())
            return orderItemOptional.get();
        return null;
    }

    @Override
    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
    }

    @Override
    public OrderItem save(OrderItem item) {
        return repository.save(item);
    }

    @Override
    public void saveAll() {
        repository.saveAll(orderItems);
        orderItems = null;
    }

    @Override
    public OrderItem update(OrderItem item, Long id) {
        item.setId(id);
        return repository.save(item);
    }

    public void addOrderItemToLocalStorage(OrderItem newItem) {
        if(orderItems == null) {
            orderItems = new ArrayList<>();
        }
        //Check if order items list already contains given product.
        boolean isNewItem = true;
        for(int i = 0; i < orderItems.size(); i++){
            OrderItem item = orderItems.get(i);
            if(item.getProduct().getId().equals(newItem.getProduct().getId())) {
                item.setQuantity(item.getQuantity() + newItem.getQuantity());
                isNewItem = false;
            }
        }
        if(isNewItem)
            orderItems.add(newItem);
    }

    public void deleteOrderItemFromLocalStorageByProductSku(String productSku) {
        for(int i = 0; i < orderItems.size(); i++){
            if(orderItems.get(i).getProduct().getSku().equals(productSku)) {
                orderItems.remove(i);
                break;
            }
        }
    }

    public void applyOrderToItemsInLocalStorage(Order order) {
        for(int i = 0; i < orderItems.size(); i++) {
            orderItems.get(i).setOrder(order);
        }
    }

    public Float calculateTotalAmount() {
        if(orderItems != null){
            Float amount = 0f;
            for(int i = 0; i < orderItems.size(); i++){
                amount += orderItems.get(i).getProduct().getPrice() * orderItems.get(i).getQuantity();
            }
            return amount;
        }
       return 0f;
    }

}
