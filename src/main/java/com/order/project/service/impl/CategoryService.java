package com.order.project.service.impl;

import com.order.project.model.Category;
import com.order.project.repository.CategoryRepository;
import com.order.project.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryService implements ICategoryService {
    private CategoryRepository repository;

    @Autowired
    public CategoryService(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Category> findAll() {
        return (List<Category>) repository.findAll();
    }

    @Override
    public Category findById(Long id) {
        Optional<Category> categoryOptional = repository.findById(id);
        if(categoryOptional.isPresent())
            return categoryOptional.get();
        return new Category();
    }

    @Override
    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
    }

    @Override
    public Category save(Category category) {
        return repository.save(category);
    }

    @Override
    public Category update(Category category, Long id) {
        category.setId(id);
        return save(category);
    }
}
