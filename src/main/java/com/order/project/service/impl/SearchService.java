package com.order.project.service.impl;

import com.order.project.model.Order;
import com.order.project.model.OrderItem;
import com.order.project.model.Product;
import com.order.project.repository.OrderItemRepository;
import com.order.project.repository.OrderRepository;
import com.order.project.repository.ProductRepository;
import com.order.project.service.ISearchService;
import com.order.project.specifications.OrderItemSpecification;
import com.order.project.specifications.OrderSpecification;
import com.order.project.specifications.ProductSpecification;
import com.order.project.specifications.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;


@Service
public class SearchService implements ISearchService {

    private ProductRepository productRepository;
    private OrderItemRepository orderItemRepository;
    private OrderRepository orderRepository;


    @Autowired
    public SearchService(ProductRepository productRepository,
                         OrderItemRepository orderItemRepository,
                         OrderRepository orderRepository) {
        this.productRepository = productRepository;
        this.orderItemRepository = orderItemRepository;
        this.orderRepository = orderRepository;
    }


    public @ResponseBody List<Product> findProductByName(String name) {

        ProductSpecification spec =
                new ProductSpecification(new SearchCriteria("name", ":", name));

        List<Product> results = productRepository.findAll(spec);
        return results;
    }

    public @ResponseBody List<OrderItem> findItemByProductName(String name) {
        OrderItemSpecification spec =
                new OrderItemSpecification(new SearchCriteria("product", ":", name));
        List<OrderItem> results = orderItemRepository.findAll(spec);
        return results;
    }

    public @ResponseBody List<Order> findOrderByProductName(String name) {
        OrderItemSpecification spec =
                new OrderItemSpecification(new SearchCriteria("product", ":", name));
        List<OrderItem> results = orderItemRepository.findAll(spec);
        List<Order> orders = new ArrayList<>();
        for(OrderItem item : results) {
            if(!orders.contains(item.getOrder())) {
                orders.add(item.getOrder());
            }
        }
        return orders;
    }
}
