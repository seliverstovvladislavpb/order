package com.order.project.service.impl;

import com.order.project.model.Order;
import com.order.project.repository.OrderRepository;
import com.order.project.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class OrderService implements IOrderService {
    private OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public List<Order> findAll() {
        return (List<Order>) orderRepository.findAll();
    }

    @Override
    public Order findById(Long id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if(optionalOrder.isPresent())
            return optionalOrder.get();
        return null;
    }

    @Override
    public void deleteById(Long id) {
        if(orderRepository.existsById(id))
            orderRepository.deleteById(id);
    }

    @Override
    public Order createOrder(Float totalAmount) {
        Order order = new Order();
        order.setDate(LocalDate.now());
        order.setAmount(totalAmount);
        return orderRepository.save(order);
    }

    @Override
    public Order update(Order order, Long id) {
        return null;
    }

    @Override
    public Map<LocalDate, Float> getReportData() {
        Map<LocalDate, Float> reportMap = new HashMap<>();
        List<Order> orders = findAll();
        for(Order order : orders) {
            if(reportMap.get(order.getDate()) == null) {
                reportMap.put(order.getDate(), order.getAmount());
            }
            else {
                reportMap.put(order.getDate(), reportMap.get(order.getDate()) + order.getAmount());
            }
        }
        return reportMap;
    }
}
