package com.order.project.service;

import com.order.project.model.Product;

import java.util.List;

public interface IProductService {

    List<Product> findAll();
    Product findById(Long id);
    void deleteById(Long id);
    Product save(Product product);
    Product update(Product product, Long id);
}
