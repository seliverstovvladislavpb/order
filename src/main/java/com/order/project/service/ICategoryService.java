package com.order.project.service;

import com.order.project.model.Category;

import java.util.List;

public interface ICategoryService {

    List<Category> findAll();
    Category findById(Long id);
    void deleteById(Long id);
    Category save(Category category);
    Category update(Category category, Long id);

}
